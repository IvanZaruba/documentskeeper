﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DocumentsKeeper.Data.Abstractions;
using DocumentsKeeper.Data.FileSystem;
using DocumentsKeeper.Services.Commands;
using DocumentsKeeper.Services.Commands.Abstractions;
using SimpleInjector;

namespace DocumentsKeepe.App
{
    internal class Program
    {
        private static void Main()
        {
            var container = new Container();

            container.RegisterCollection<AbstractCommand>(new Type[] 
            {
                typeof(AddDocumentCommand),
                typeof(GetDocumentCommand),
                typeof(DeleteDocumentCommand)
            });
            
            container.Register<IDataProvider, FileSystemProvider>();
            container.Register<IDocumentProvider, FileDocumentProvider>();
            container.Register<CommandHandler>();

            container.Verify();

            var commandHandler = container.GetInstance<CommandHandler>();

            while (true)
            {
                Console.Write("> ");

                var input = Console.ReadLine().Split(' ');
                var command = input[0];
                var args = input.Skip(1).ToArray();

                commandHandler.Handle(command, args);
            }
        }
    }
}
