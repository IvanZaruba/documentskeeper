﻿using System.Collections.Generic;

namespace DocumentsKeeper.Data.Abstractions
{
    public interface IDataProvider
    {
        void Save(string name, string text);

        bool TryRead(string name, out string text);

        void Delete(string name);

        IEnumerable<string> Documents { get; }
    }
}