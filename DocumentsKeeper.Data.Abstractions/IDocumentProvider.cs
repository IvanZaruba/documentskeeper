﻿using System.Collections.Generic;
using DocumentsKeeper.Domain;

namespace DocumentsKeeper.Data.Abstractions
{
    public interface IDocumentProvider
    {
        IDocument Get(string name);

        void Delete(string name);

        void Save(IDocument document);

        IEnumerable<IDocument> Documents { get; }
    }
}