﻿using System;
using System.Collections.Generic;
using DocumentsKeeper.Data.Abstractions;
using DocumentsKeeper.Domain;

namespace DocumentsKeeper.Data.FileSystem
{
    public class FileDocumentProvider : IDocumentProvider
    {
        private readonly IDataProvider dataProvider;

        public FileDocumentProvider(IDataProvider dataProvider)
        {
            this.dataProvider = dataProvider;
        }

        public IDocument Get(string name)
        {
            var exists = this.dataProvider.TryRead(name, out var text);

            if (!exists) return null;

            return new FileDocument
            {
                Name = name,
                Text = text
            };
        }

        public void Delete(string name)
        {
            this.dataProvider.Delete(name);
        }

        public void Save(IDocument document)
        {
            this.dataProvider.Save(document.Name, document.Text);
        }

        public IEnumerable<IDocument> Documents =>
            throw new NotImplementedException();
    }
}