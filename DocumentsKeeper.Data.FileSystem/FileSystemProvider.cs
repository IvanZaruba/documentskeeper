﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using DocumentsKeeper.Data.Abstractions;
using DocumentsKeeper.Domain;

namespace DocumentsKeeper.Data.FileSystem
{
    public class FileSystemProvider : IDataProvider
    {
        private readonly string baseDirectory =
            Path.Combine(
                Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
                "data");

        public FileSystemProvider()
        {
            if (!Directory.Exists(this.baseDirectory))
            {
                Directory.CreateDirectory(this.baseDirectory);
            }
        }

        public void Save(string name, string text)
        {
            var docFileName = $"{name}{FileDocument.FileExtension}";

            var filePath = Path.Combine(this.baseDirectory, docFileName);

            File.WriteAllText(filePath, text);
        }

        public bool TryRead(string name, out string text)
        {
            var docFileName = $"{name}{FileDocument.FileExtension}";
            var filePath = Path.Combine(this.baseDirectory, docFileName);
            var fileExists = File.Exists(filePath);

            text = fileExists
                ? File.ReadAllText(filePath)
                : null;

            return fileExists;
        }

        public void Delete(string name)
        {
            var docFileName = $"{name}{FileDocument.FileExtension}";
            var filePath = Path.Combine(this.baseDirectory, docFileName);

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        public IEnumerable<string> Documents =>
            Directory.EnumerateFiles(this.baseDirectory)
                .Select(Path.GetFileNameWithoutExtension)
                .AsEnumerable();
    }
}