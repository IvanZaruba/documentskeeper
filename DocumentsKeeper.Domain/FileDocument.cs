﻿namespace DocumentsKeeper.Domain
{
    public class FileDocument : Document
    {
        public const string FileExtension = ".json";
    }
}