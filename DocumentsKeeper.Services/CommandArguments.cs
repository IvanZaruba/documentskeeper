﻿namespace DocumentsKeeper.Services
{
    public class CommandArguments
    {
        public const string Text = "--text";

        public const string DocumentName = "--name";
    }
}