﻿namespace DocumentsKeeper.Services.Commands.Abstractions
{
    public interface ICommand
    {
        void Execute(string[] args);
    }
}