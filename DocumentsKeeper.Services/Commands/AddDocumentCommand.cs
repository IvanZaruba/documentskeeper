﻿using System;
using DocumentsKeeper.Data.Abstractions;
using DocumentsKeeper.Domain;
using DocumentsKeeper.Services.Commands.Abstractions;

namespace DocumentsKeeper.Services.Commands
{
    public class AddDocumentCommand : AbstractCommand
    {
        private readonly IDocumentProvider documentProvider;

        public AddDocumentCommand(IDocumentProvider documentProvider)
        {
            this.documentProvider = documentProvider;
        }

        public override string Name { get; set; } = "add";

        public override void Execute(string[] args)
        {
            var knownArguments = this.ParseArguments(args);
            var doc = new FileDocument();

            if (knownArguments.TryGetValue(CommandArguments.DocumentName, out var docName))
            {
                doc.Name = docName;
            }
            else
            {
                Console.WriteLine($"Please specify document name (e.g. {CommandArguments.DocumentName} <name>)");
                return;
            }

            if (knownArguments.TryGetValue(CommandArguments.Text, out var text))
            {
                doc.Text = text;
            }

            this.documentProvider.Save(doc);
        }
    }
}