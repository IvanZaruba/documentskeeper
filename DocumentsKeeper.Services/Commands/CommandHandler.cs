﻿using System;
using System.Collections.Generic;
using DocumentsKeeper.Services.Commands.Abstractions;

namespace DocumentsKeeper.Services.Commands
{
    public class CommandHandler
    {
        private readonly Dictionary<string, AbstractCommand> commands = 
            new Dictionary<string, AbstractCommand>();

        public CommandHandler(IEnumerable<AbstractCommand> commands)
        {
            foreach (var command in commands)
            {
                this.RegisterCommand(command);
            }
        }

        public void Handle(string command, string[] args)
        {
            if (this.commands.TryGetValue(command, out var cmd))
            {
                cmd.Execute(args);
            }
            else
            {
                Console.WriteLine($"Unknown command {command}");
            }
        }

        protected void RegisterCommand(AbstractCommand command)
        {
            this.commands[command.Name] = command;
        }
    }
}