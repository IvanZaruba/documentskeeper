﻿using System;
using DocumentsKeeper.Data.Abstractions;
using DocumentsKeeper.Services.Commands.Abstractions;

namespace DocumentsKeeper.Services.Commands
{
    public class DeleteDocumentCommand : AbstractCommand
    {
        private readonly IDocumentProvider documentProvider;

        public DeleteDocumentCommand(IDocumentProvider documentProvider)
        {
            this.documentProvider = documentProvider;
        }

        public override string Name { get; set; } = "del";

        public override void Execute(string[] args)
        {
            var knownArguments = this.ParseArguments(args);

            if (knownArguments.TryGetValue(CommandArguments.DocumentName, out var docName))
            {
                this.documentProvider.Delete(docName);
            }
            else
            {
                Console.WriteLine($"Please specify document name (e.g. {CommandArguments.DocumentName} <name>)");
            }
        }
    }
}