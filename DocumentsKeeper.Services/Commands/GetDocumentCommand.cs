﻿using System;
using DocumentsKeeper.Data.Abstractions;
using DocumentsKeeper.Services.Commands.Abstractions;

namespace DocumentsKeeper.Services.Commands
{
    public class GetDocumentCommand : AbstractCommand
    {
        private readonly IDocumentProvider documentProvider;

        public GetDocumentCommand(IDocumentProvider documentProvider)
        {
            this.documentProvider = documentProvider;
        }

        public override string Name { get; set; } = "get";

        public override void Execute(string[] args)
        {
            var knownArguments = this.ParseArguments(args);

            if (knownArguments.TryGetValue(CommandArguments.DocumentName, out var docName))
            {
                var doc = this.documentProvider.Get(docName);

                if (doc == null)
                {
                    Console.WriteLine($"Document {docName} doesn't exist");
                    return;
                }

                Console.WriteLine(doc.Text);
            }
            else
            {
                Console.WriteLine($"Please specify document name (e.g. {CommandArguments.DocumentName} <name>)");
            }
        }
    }
}