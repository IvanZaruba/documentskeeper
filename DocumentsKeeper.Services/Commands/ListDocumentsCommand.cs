﻿using System;
using System.Linq;
using DocumentsKeeper.Data.Abstractions;
using DocumentsKeeper.Services.Commands.Abstractions;

namespace DocumentsKeeper.Services.Commands
{
    public class ListDocumentsCommand : AbstractCommand
    {
        private readonly IDocumentProvider documentProvider;

        public ListDocumentsCommand(IDocumentProvider documentProvider)
        {
            this.documentProvider = documentProvider;
        }

        public override string Name { get; set; } = "list";

        public override void Execute(string[] args)
        {
            var docs = this.documentProvider.Documents
                .Select(doc => doc.Name)
                .ToArray();

            Array.ForEach(docs, Console.WriteLine);
        }
    }
}